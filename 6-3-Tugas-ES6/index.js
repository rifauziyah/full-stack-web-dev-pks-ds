// Soal 1
// Fungsi menggunakan arrow function luas dan keliling persegi panjang
const luas = () => {
    return p * l
}

const keliling = () => {
    return 2 * (p + l)
}

let p = 8
let l = 5

console.log(luas())
console.log(keliling())


// Soal 2
// Mengubah code di bawah menjadi arrow function dan object literal es6
/*
const newFunction = function literal(firstName, lastName){
    return {
        firstName: firstName,
        lastName: lastName,
        fullName: function(){
        console.log(firstName + " " + lastName)
        }
    }
}
*/

const newFunction = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        fullName : () => {
            console.log(`${firstName} ${lastName}`)
        }
    }
}

//Driver Code 
newFunction("William", "Imoh").fullName()


// Soal 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}

// ES5
/*
const firstName = newObject.firstName;
const lastName = newObject.lastName;
const address = newObject.address;
const hobby = newObject.hobby;
*/

// Menggunakan metode destructuring dalam ES6
const {firstName, lastName, address, hobby} = newObject

// Driver code
console.log(firstName, lastName, address, hobby)


// Soal 4
// Mengkombinasikan dua array dengan menggunakan array spreading ES6
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
// const combined = west.concat(east)

const combined = [...west, ...east]

//Driver Code
console.log(combined)


// Soal 5
// Menyederhanakan string dengan menggunakan template literals ES6
const planet = "earth" 
const view = "glass" 
// var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 

const before = `Lorem ${view} dolor sit amat, consectetur adipiscing elit, ${planet}`
console.log(before)