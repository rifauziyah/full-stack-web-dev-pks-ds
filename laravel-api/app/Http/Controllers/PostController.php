<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->except('index', 'show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //mengambil data dari tabel posts
        $posts = Post::latest()->get();

        //respons JSON
        return response()->json([
            'success' => true,
            'message' => 'Daftar data post berhasil ditampilkan',
            'data'    => $posts  
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'title'       => 'required',
            'description' => 'required',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        // $user = auth()->user();

        //simpan data ke database
        $post = Post::create([
            'title'       => $request->title,
            'description' => $request->description,
            // 'user_id'     => $user->id
        ]);

        //berhasil menyimpan ke database
        if($post) {
            return response()->json([
                'success' => true,
                'message' => 'Data Post berhasil dibuat',
                'data'    => $post  
            ], 201);
        }

        //gagal menyimpan ke database
        return response()->json([
            'success' => false,
            'message' => 'Data Post gagal dibuat',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //find post by ID
        $post = Post::find($id);

        if($post)
        {
            //respons JSON
            return response()->json([
                'success' => true,
                'message' => 'Detail Post berhasil ditampilkan',
                'data'    => $post 
            ], 200);
        }

        //data post tidak ditemukan
        return response()->json([
            'success' => false,
            'message' => 'Post tidak ditemukan',
        ], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'title'   => 'required',
            'description' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        $post = Post::find($id);

        if($post) {

            $user = auth()->user();

            if($post->user_id != $user->id)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Data post bukan milik user login',
                ] , 403);

            }

            //update post
            $post->update([
                'title'         => $request->title,
                'description'   => $request->description
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Post berhasil diperbaharui',
                'data'    => $post  
            ], 200);
        }

        //data post tidak ditemukan
        return response()->json([
            'success' => false,
            'message' => 'Post tidak ditemukan',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //find post by ID
        $post = Post::find($id);

        if($post) {

            $user = auth()->user();

            if($post->user_id != $user->id)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Data post bukan milik user login',
                ] , 403);

            }

            //delete post
            $post->delete();

            return response()->json([
                'success' => true,
                'message' => 'Post berhasil dihapus',
            ], 200);
        }

        //data post tidak ditemukan
        return response()->json([
            'success' => false,
            'message' => 'Post tidak ditemukan',
        ], 404);
    }
}
