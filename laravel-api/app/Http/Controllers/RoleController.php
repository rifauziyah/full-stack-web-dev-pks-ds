<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::latest()->get();

        //respons JSON
        return response()->json([
            'success' => true,
            'message' => 'Daftar data role berhasil ditampilkan',
            'data'    => $roles  
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //simpan data ke database
        $role = Role::create([
            'name' => $request->name,
        ]);

        //berhasil menyimpan ke database
        if($role) {
            return response()->json([
                'success' => true,
                'message' => 'Data Role berhasil dibuat',
                'data'    => $role  
            ], 201);
        }

        //gagal menyimpan ke database
        return response()->json([
            'success' => false,
            'message' => 'Data Role gagal dibuat',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //find role by ID
        $role = Role::find($id);

        if($role)
        {
            //respons JSON
            return response()->json([
                'success' => true,
                'message' => 'Detail Role berhasil ditampilkan',
                'data'    => $role 
            ], 200);
        }

        //data role tidak ditemukan
        return response()->json([
            'success' => false,
            'message' => 'Role tidak ditemukan',
        ], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find role by ID
        $role = Role::find($id);

        if($role) {
            //update role
            $role->update([
                'name' => $request->name,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Role berhasil diperbaharui',
                'data'    => $role  
            ], 200);
        }

        //data role tidak ditemukan
        return response()->json([
            'success' => false,
            'message' => 'Role tidak ditemukan',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //find role by ID
        $role = Role::find($id);

        if($role) {
            //delete role
            $role->delete();

            return response()->json([
                'success' => true,
                'message' => 'Role berhasil dihapus',
            ], 200);
        }

        //data role tidak ditemukan
        return response()->json([
            'success' => false,
            'message' => 'Role tidak ditemukan',
        ], 404);
    }
}
