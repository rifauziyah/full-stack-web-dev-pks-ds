<?php

namespace App\Http\Controllers;

use App\Comment;
// use App\Mail\PostAuthorMail;
use Illuminate\Http\Request;
// use App\Mail\CommentAuthorMail;
use App\Events\CommentStoredEvent;
// use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->except('index', 'show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comments = Comment::latest()->get();

        //respons JSON
        return response()->json([
            'success' => true,
            'message' => 'Daftar data komentar berhasil ditampilkan',
            'data'    => $comments  
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'content' => 'required',
            'post_id' => 'required',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //simpan data ke database
        $comment = Comment::create([
            'content' => $request->content,
            'post_id' => $request->post_id,
        ]);

        //memanggil event CommentStoredEvent
        event(new CommentStoredEvent($comment));

        //mengirim email ke post author
        // Mail::to($comment->post->user->email)->send(new PostAuthorMail($comment));

        //mengirim email ke comment author
        // Mail::to($comment->user->email)->send(new CommentAuthorMail($comment));

        //berhasil menyimpan ke database
        if($comment) {
            return response()->json([
                'success' => true,
                'message' => 'Komentar berhasil dibuat',
                'data'    => $comment
            ], 201);
        }

        //gagal menyimpan ke database
        return response()->json([
            'success' => false,
            'message' => 'Komentar gagal dibuat',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //find post by ID
        $comment = Comment::find($id);

        if($comment)
        {
            //respons JSON
            return response()->json([
                'success' => true,
                'message' => 'Detail komentar berhasil ditampilkan',
                'data'    => $comment 
            ], 200);
        }

        //data post tidak ditemukan
        return response()->json([
            'success' => false,
            'message' => 'Komentar tidak ditemukan',
        ], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'content' => 'required',
            'post_id' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find comment by ID
        $comment = Comment::find($id);

        if($comment) {

            $user = auth()->user();

            if ($comment->user_id != $user->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'Data comment bukan milik user login',
                ], 403);
            }

            //update komentar
            $comment->update([
                'content' => $request->content,
                'post_id' => $request->post_id,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Komentar berhasil diperbaharui',
                'data'    => $comment  
            ], 200);
        }

        //data komentar tidak ditemukan
        return response()->json([
            'success' => false,
            'message' => 'Komentar tidak ditemukan',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //find comment by ID
        $comment = Comment::find($id);

        if($comment) {

            $user = auth()->user();

            if ($comment->user_id != $user->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'Data comment bukan milik user login',
                ], 403);
            }

            //delete comment
            $comment->delete();

            return response()->json([
                'success' => true,
                'message' => 'Komentar berhasil dihapus',
            ], 200);
        }

        //data komentar tidak ditemukan
        return response()->json([
            'success' => false,
            'message' => 'Komentar tidak ditemukan',
        ], 404);
    }
}
