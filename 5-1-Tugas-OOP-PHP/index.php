<?php

abstract class Hewan
{
    protected $jenisHewan, $darah = 50, $jumlahKaki, $keahlian;

    public function getInfoHewan()
    {
        echo "Jenis Hewan = {$this->jenisHewan} <br>";
        echo "Jumlah Kaki = {$this->jumlahKaki} <br>";
        echo "Keahlian = {$this->keahlian} <br>";
        echo "Darah = {$this->darah} <br>";
        echo "Attack Power = {$this->attackPower} <br>";
        echo "Defence Power = {$this->defencePower} <br>";
    }

    public function atraksi()
    {
        echo "{$this->jenisHewan} sedang {$this->keahlian}<br>";
    }
}


abstract class Fight extends Hewan
{
    protected $attackPower, $defencePower;

    public function serang($lawan)
    {
        echo "{$this->jenisHewan} sedang menyerang {$lawan->jenisHewan}<br>";
        $this->diserang($lawan);
    }

    private function diserang($lawan)
    {
        echo "$lawan->jenisHewan sedang diserang<br>";
        $lawan->darah -= ($this->attackPower / $lawan->defencePower);
    }

    public function sisaDarah()
    {
        echo "Sisa darah {$this->jenisHewan} = {$this->darah}<br>";
    }
}


class Elang extends Fight
{
    public function __construct()
    {
        $this->jenisHewan = "Elang";
        $this->jumlahKaki = 2;
        $this->keahlian = "terbang tinggi";
        $this->attackPower = 10;
        $this->defencePower = 5;
    }
}


class Harimau extends Fight
{
    public function __construct()
    {
        $this->jenisHewan = "Harimau";
        $this->jumlahKaki = 4;
        $this->keahlian = "lari cepat";
        $this->attackPower = 7;
        $this->defencePower = 8;
    }
}


$elang = new Elang;
$harimau = new Harimau;

$harimau->getInfoHewan();
echo "<br>";
$elang->getInfoHewan();
echo "<hr>";

$harimau->atraksi();
$elang->atraksi();
echo "<br>";

$elang->serang($harimau);
echo "<br>";

$harimau->sisaDarah();
$elang->sisaDarah();
echo "<br>";

$harimau->serang($elang);
echo "<br>";

$harimau->sisaDarah();
$elang->sisaDarah();