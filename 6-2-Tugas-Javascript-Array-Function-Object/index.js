// Soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

for (var i = 0; i < daftarHewan.length; i++) {
    daftarHewan.sort()
    console.log(daftarHewan[i])
}

console.log()

// Soal 2
function introduce(item){
    return "Nama saya " + item.name + ", umur saya " + item.age + " tahun, alamat saya di " + item.address + ", dan saya punya hobby yaitu " + item.hobby
}

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)
console.log(perkenalan)

console.log()

// Soal 3
function hitung_huruf_vokal(str){
    var vokal = str.match(/[aiueo]/gi);
    return vokal === null ? 0 : vokal.length;
}

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2) // 3 2
console.log()

// Soal 4
function hitung(angka){
    return (angka * 2) - 2
}

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8